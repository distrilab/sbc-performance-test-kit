#!/bin/bash

if [[ $# -lt 3 ]] ; then
  echo "\\o/"
  echo "Not enough arguments supplied" >&2
  echo "You need to pass a log file name to store the test results, the disk space to use and the number of files to use " >&2
  echo " |" >&2
  echo "syntax: " >&2
  echo "  $(basename $0) <log file name> <test disk space> <test file num>" >&2
  echo "example: " >&2
  echo "  $(basename $0) test.log 64G 512 " >&2
  echo "/!\\" >&2
  exit 1
fi

LOGFILE=$1
TEST_DISK_SPACE=$2
TEST_FILE_NUM=$3

#FILE_IO_MODE=async
FILE_IO_MODE=sync
THREADS=8
BLOCK_SIZE_1=4096
BLOCK_SIZE_2=1048576
BLOCK_SIZE_1_TXT="4 kiB"
BLOCK_SIZE_2_TXT="1 MiB"

echo ' '
echo "#### Disk test ####" | tee -a $LOGFILE

BLOCK_DEVICES=$(lsblk | grep -v loop | grep disk | cut -f1 -d ' ')
for d in $BLOCK_DEVICES
do
  echo "# block device /dev/$d => hdparm -tT #" | tee -a $LOGFILE
  sudo hdparm -tT /dev/$d 2>&1 | tee -a $LOGFILE
  sudo hdparm -tT /dev/$d 2>&1 | tee -a $LOGFILE
  sudo hdparm -tT /dev/$d 2>&1 | tee -a $LOGFILE
done

/usr/local/bin/sysbench fileio prepare --file-num=$TEST_FILE_NUM --file-total-size=$TEST_DISK_SPACE 2>&1 | tee -a $LOGFILE
sudo sh -c "echo 1 > /proc/sys/vm/drop_caches" 2>&1 | tee -a $LOGFILE
echo "# sysbench $BLOCK_SIZE_1_TXT random read/write test #" | tee -a $LOGFILE
/usr/local/bin/sysbench fileio run --threads=$THREADS --file-test-mode=rndrw --file-block-size=$BLOCK_SIZE_1 --file-io-mode=$FILE_IO_MODE --file-num=$TEST_FILE_NUM --file-total-size=$TEST_DISK_SPACE --file-rw-ratio=2 2>&1 | tee -a $LOGFILE
/usr/local/bin/sysbench fileio prepare --file-num=$TEST_FILE_NUM --file-total-size=$TEST_DISK_SPACE 2>&1 | tee -a $LOGFILE
sudo sh -c "echo 1 > /proc/sys/vm/drop_caches" 2>&1 | tee -a $LOGFILE
echo "# sysbench $BLOCK_SIZE_2_TXT random read/write test #" | tee -a $LOGFILE
/usr/local/bin/sysbench fileio run --threads=$THREADS --file-test-mode=rndrw --file-block-size=$BLOCK_SIZE_2 --file-io-mode=$FILE_IO_MODE --file-num=$TEST_FILE_NUM --file-total-size=$TEST_DISK_SPACE --file-rw-ratio=2 2>&1 | tee -a $LOGFILE

/usr/local/bin/sysbench fileio prepare --file-num=$TEST_FILE_NUM --file-total-size=$TEST_DISK_SPACE 2>&1 | tee -a $LOGFILE
sudo sh -c "echo 1 > /proc/sys/vm/drop_caches" 2>&1 | tee -a $LOGFILE
echo "# sysbench $BLOCK_SIZE_1_TXT sequential read test #" | tee -a $LOGFILE
/usr/local/bin/sysbench fileio run --threads=$THREADS --file-test-mode=seqrd --file-block-size=$BLOCK_SIZE_1 --file-io-mode=$FILE_IO_MODE --file-num=$TEST_FILE_NUM --file-total-size=$TEST_DISK_SPACE 2>&1 | tee -a $LOGFILE
/usr/local/bin/sysbench fileio prepare --file-num=$TEST_FILE_NUM --file-total-size=$TEST_DISK_SPACE 2>&1 | tee -a $LOGFILE
sudo sh -c "echo 1 > /proc/sys/vm/drop_caches" 2>&1 | tee -a $LOGFILE
echo "# sysbench $BLOCK_SIZE_2_TXT sequential read test #" | tee -a $LOGFILE
/usr/local/bin/sysbench fileio run --threads=$THREADS --file-test-mode=seqrd --file-block-size=$BLOCK_SIZE_2 --file-io-mode=$FILE_IO_MODE --file-num=$TEST_FILE_NUM --file-total-size=$TEST_DISK_SPACE 2>&1 | tee -a $LOGFILE

sudo sh -c "echo 1 > /proc/sys/vm/drop_caches" 2>&1 | tee -a $LOGFILE
echo "# sysbench $BLOCK_SIZE_1_TXT sequential write test #" | tee -a $LOGFILE
/usr/local/bin/sysbench fileio run --threads=$THREADS --file-test-mode=seqwr --file-block-size=$BLOCK_SIZE_1 --file-io-mode=$FILE_IO_MODE --file-num=$TEST_FILE_NUM --file-total-size=$TEST_DISK_SPACE 2>&1 | tee -a $LOGFILE
sudo sh -c "echo 1 > /proc/sys/vm/drop_caches" 2>&1 | tee -a $LOGFILE
echo "# sysbench $BLOCK_SIZE_2_TXT sequential write test #" | tee -a $LOGFILE
/usr/local/bin/sysbench fileio run --threads=$THREADS --file-test-mode=seqwr --file-block-size=$BLOCK_SIZE_2 --file-io-mode=$FILE_IO_MODE --file-num=$TEST_FILE_NUM --file-total-size=$TEST_DISK_SPACE 2>&1 | tee -a $LOGFILE

/usr/local/bin/sysbench fileio cleanup --file-num=$TEST_FILE_NUM --file-total-size=$TEST_DISK_SPACE 2>&1 | tee -a $LOGFILE

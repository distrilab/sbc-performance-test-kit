---
title: ODroid H2+ benchmark results
bookCollapseSection: true
weight: 10
---

Benchmark results summary for the ODroid H2+

Content :

[[_TOC_]]

## Setup tested

- [ODroid H2+ board](https://www.hardkernel.com/shop/odroid-h2plus/)
- [2x Samsung 16GB DDR4 PC4-19200 SO-DIMM](https://www.hardkernel.com/shop/samsung-16gb-ddr4-pc4-19200-so-dimm/)
- [128GB emmc module](https://www.hardkernel.com/shop/128gb-emmc-module-h2-2/)
- [2x 2TB Crucial MX500 SATA SSD](https://www.crucial.com/products/ssd/crucial-mx500-ssd)
- [15V/4A power supply](https://www.hardkernel.com/shop/15v-4a-power-supply-eu-plug/) 
- [Cooling fan](https://www.hardkernel.com/shop/92x92x25mm-dc-cooling-fan-w-pwm-speed-sensor-tacho/)
- [Type 3 case](https://www.hardkernel.com/shop/odroid-h2-case-3/)


## Power consumption
Between 8 W.h consumed during the 1 hour benchmark.

Power used when idle is between 3.8 and 6.4 W
- Depends if keyboard/mouse and HDMI screen is connected. Unplugging them reduces the power consumption by 1.6 W
- Seems to also depends on other parameters not yet identified. 
- When idle with no mouse, no keyboard, no screen and only one LAN cable connected, I had it consistently using less than 4W before. But it is currently using 4.8 W
- Maybe the linux distribution used and its default power saving settings makes a difference. More tests needed.

Power used under CPU load is between 10.1 and 15 W.


## CPU/Memory performance

CPU : 5752 events/s
Memory : 7698.35 MiB/sec

CPU temperature under max load was ocillating between 60 and 62 °C and case temperature was kep between 38 and 40°C
The fan was cycling between idle and 1000 RPM aroud every 2 minutes.

Max observed temperatures :
```
coretemp-isa-0000
Adapter: ISA adapter
Package id 0:  +62.0°C  (high = +105.0°C, crit = +105.0°C)
Core 0:        +62.0°C  (high = +105.0°C, crit = +105.0°C)
Core 1:        +62.0°C  (high = +105.0°C, crit = +105.0°C)
Core 2:        +62.0°C  (high = +105.0°C, crit = +105.0°C)
Core 3:        +62.0°C  (high = +105.0°C, crit = +105.0°C)

acpitz-acpi-0
Adapter: ACPI interface
temp1:        +62.0°C  (crit = +95.0°C)

emc2103-i2c-0-2e
Adapter: SMBus I801 adapter at f040
fan1:        1006 RPM  (div = 8)
temp1:        +40.5°C  (low  =  +0.0°C, high = +85.0°C)
temp2:        +40.0°C  (low  =  +0.0°C, high = +85.0°C)
temp3:        +39.6°C  (low  =  +0.0°C, high = +85.0°C)
```

Notes :
- sysbench CPU and memory test performed with 8 thread 
- ambiant temperature during the test was 21 °C
- in the bios the fan was set to start at 40°C with 2°C hysteresis. 
- no thermal throttling was noticed during the CPU stress (sysbench CPU event/s remained constant .


## Disk performance
### Cached reads
```
 Timing cached reads:   9968 MB in  1.99 seconds = 5010.25 MB/sec
 Timing cached reads:   9318 MB in  1.99 seconds = 4681.90 MB/sec
 Timing cached reads:   9220 MB in  1.99 seconds = 4631.96 MB/sec
 Timing cached reads:   9558 MB in  1.99 seconds = 4802.68 MB/sec
 Timing cached reads:   9442 MB in  1.99 seconds = 4744.38 MB/sec
 Timing cached reads:   9358 MB in  1.99 seconds = 4702.29 MB/sec
 Timing cached reads:   9282 MB in  1.99 seconds = 4662.86 MB/sec
 Timing cached reads:   8938 MB in  1.99 seconds = 4490.37 MB/sec
 Timing cached reads:   8942 MB in  1.99 seconds = 4492.11 MB/sec
 Timing cached reads:   9272 MB in  1.99 seconds = 4659.12 MB/sec
 Timing cached reads:   9058 MB in  1.99 seconds = 4550.62 MB/sec
 Timing cached reads:   8996 MB in  1.99 seconds = 4519.27 MB/sec
 Timing cached reads:   9316 MB in  1.99 seconds = 4680.49 MB/sec
 Timing cached reads:   9046 MB in  1.99 seconds = 4544.69 MB/sec
 Timing cached reads:   8954 MB in  1.99 seconds = 4497.92 MB/sec
```

### 128GB EMMC  
**Raw Read**
```
 Timing buffered disk reads: 626 MB in  3.01 seconds = 207.96 MB/sec
 Timing buffered disk reads: 626 MB in  3.01 seconds = 207.93 MB/sec
 Timing buffered disk reads: 620 MB in  3.01 seconds = 206.20 MB/sec
```

**Random Read/Write - 4KiB block size**  
Throughput:
- read:  IOPS=1966.32 7.68 MiB/s (8.05 MB/s)
- write: IOPS=982.86 3.84 MiB/s (4.03 MB/s)

Latency (ms):
- min:      0.00
- avg:      0.44
- max:      28.27
- 95th pct: 1.14

**Random Read/Write - 1MiB block size**  
Throughput:
- read:  IOPS=123.24 123.24 MiB/s (129.22 MB/s)
- write: IOPS=61.33 61.33 MiB/s (64.31 MB/s)

Latency (ms):
- min:      0.05
- avg:      7.16
- max:      328.49
- 95th pct: 38.25

**Sequential Read - 4KiB block size**  
Throughput:
- read:  IOPS=85473.17 333.88 MiB/s (350.10 MB/s)

Latency (ms):
- min:      0.00
- avg:      0.09
- max:      1.89
- 95th pct: 0.37

**Sequential Read - 1MiB block size**  
Throughput:
- read:  IOPS=321.46 321.46 MiB/s (337.08 MB/s)

Latency (ms):
- min:      13.09
- avg:      24.86
- max:      29.72
- 95th pct: 27.17

**Sequential Write - 4KiB block size**  
Throughput:
 write: IOPS=6682.10 26.10 MiB/s (27.37 MB/s)

Latency (ms):
- min:      0.01
- avg:      0.19
- max:      23.99
- 95th pct: 0.26

**Sequential Write - 1MiB block size**  
Throughput:
- write: IOPS=107.87 107.87 MiB/s (113.11 MB/s)

Latency (ms):
- min:      0.06
- avg:      12.93
- max:      845.49
- 95th pct: 94.10

### 2TB MX500 USB2
**Random Read/Write - 4KiB block size**  
**Random Read/Write - 1MiB block size**  
**Sequential Read - 4KiB block size**  
**Sequential Read - 1MiB block size**  
**Sequential Write - 4KiB block size**  
**Sequential Write - 1MiB block size**  

### 2TB MX500 USB3
**Random Read/Write - 4KiB block size**  
**Random Read/Write - 1MiB block size**  
**Sequential Read - 4KiB block size**  
**Sequential Read - 1MiB block size**  
**Sequential Write - 4KiB block size**  
**Sequential Write - 1MiB block size**  

### 2TB MX500 SATA
**Random Read/Write - 4KiB block size**  
**Random Read/Write - 1MiB block size**  
**Sequential Read - 4KiB block size**  
**Sequential Read - 1MiB block size**  
**Sequential Write - 4KiB block size**  
**Sequential Write - 1MiB block size**  

### 2x 2TB MX500 SATA in RAID1
**Random Read/Write - 4KiB block size**  
**Random Read/Write - 1MiB block size**  
**Sequential Read - 4KiB block size**  
**Sequential Read - 1MiB block size**  
**Sequential Write - 4KiB block size**  
**Sequential Write - 1MiB block size**  

### 2x 2TB MX500 SATA in RAID0
**Random Read/Write - 4KiB block size**  
**Random Read/Write - 1MiB block size**  
**Sequential Read - 4KiB block size**  
**Sequential Read - 1MiB block size**  
**Sequential Write - 4KiB block size**  
**Sequential Write - 1MiB block size**  
